﻿using UnityEngine;
using System.Collections;

public class AniMenuInicial : MonoBehaviour {

	public AudioSource audioAndrew;
	float cont;
	int testebacana;
	float testeMaisBacanaAinda;
	//public Canvas creditos;
	GameObject img1;
	GameObject img2;
	// Use this for initialization
	void Start () {
		audioAndrew.Play ();
		img1 = GameObject.Find ("Frames1");
		img1.SetActive (true);
		img2 = GameObject.Find ("Frames2");
		img2.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		cont += Time.deltaTime;
		if (cont > 15) {
			img1.SetActive (false);
			img2.SetActive (true);
		}

	}
}
